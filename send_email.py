import os, sys
import smtplib
from configparser import ConfigParser
from email import encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.utils import formatdate
import allure
import pytest

def test_send_mail():
    encode ='utf-8'
    base_path = os.path.dirname(os.path.abspath(__file__))
    cfg.read(config_path, encoding = encode)
    # проверить наличие файла 'email.ini'
    with allure.step("проверить наличие файла 'email.ini'"):
        if os.path.exists(config_path):
            cfg = ConfigParser()
            cfg.read(config_path)
            with allure.step ("извлечь переменные из файла"):
                server = cfg.get("smtp", "server")
                port = int(cfg.get("smtp", "port"))
                from_email = cfg.get("smtp", "email")
                password = cfg.get("smtp", "passwd")
                to_email = cfg.get("smtp", "to_email")
                subject = cfg.get("smtp", "subject")
                text = cfg.get("smtp", "text")
                file_to_attach = cfg.get("smtp", "file_to_attach")
                cc_email = cfg.get("smtp", "cc_email")

        else:
            print("Не найдена конфигурация")
            sys.exit(1)

    charset = f'Content -Type:text/plan; charset={encode}'
    mime = 'MIME-version: 1.0'
    # Тело письма
    with allure.step("Тело письма"):
        body = "\r\n".join((f"From: {from_email}", f"To: {','.join(to_email)}",
                        f"CC: {'. '.join(cc_email)}",
                        f"Subject: {subject}", mime, charset, "", text))
    all_emails = to_email + cc_email
    attachment = MIMEBase('application', "octet-stream")
    header = 'Content-Disposition', f'attachment; filename="{file_to_attach}"'
    try:
        # подключиться к почтовому серверу
        with allure.step("подключиться к почтовому серверу"):
            smtp = smtplib.SMTP(server, port)
            smtp.starttls()
            smtp.ehlo()
            # пройти  авторизацию на  сервере
        with allure.step("пройти  авторизацию на  сервере"):
            smtp.login(from_email, password)
        # отправить  письмо
        with allure.step("отправить  письмо"):
            smtp.sendmail(from_email, to_email, body.encode(encode))
    except smtplib.SMPTException as error:
        print('Что то не так....')
        raise error
    finally:
        smtp.quit()


if __name__ == "__main__":
    send_mail()
